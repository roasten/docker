#!/bin/sh
cd /app
git clone git@bitbucket.org:archiveonline/repository-package.git repository-package
echo "done git 1\n"
git clone git@bitbucket.org:archiveonline/tenant-package.git tenant-package
echo "done git 2\n"
git clone git@bitbucket.org:archiveonline/tenant-application.git tenant-application
echo "done git 3\n"
git clone git@bitbucket.org:archiveonline/ao-frontend.git ao-frontend
echo "done git 4\n"

cd tenant-application
#composer install 
composer config -g github-oauth.github.com 720184567d8f283c54faa69f1b02d6179d8ade34
composer update --no-scripts --no-interaction --prefer-dist
composer update

cp .env.example .env
mysql -h mysql --user="root" --password="" --execute="CREATE DATABASE IF NOT EXISTS tenant" 2>/dev/null

php artisan key:generate
php artisan migrate
php artisan db:seed
#php artisan serve &
echo "Go to http://localhost:8080/tentant-application/public"

cd ao-frontend
npm install
bower install  --allow-root 
gulp build


# c0999788edd6f5760e3d9a3b1739007c691aba0e
# Remove composer.lock