FROM ubuntu:latest

MAINTAINER Uppdragshuset version 1.0 <it@uppdragshuset.se>

RUN apt-get update && apt-get install software-properties-common -y
RUN LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php -y
RUN apt-get update && apt-get install -y --force-yes php7.0 php7.0-fpm php7.0-mysql
RUN apt-get install -y curl && curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer
RUN apt-get install -y nginx
RUN apt-get install nano
RUN apt-get install -y mysql-client
RUN apt-get install -y git
RUN apt-get install -y nodejs-legacy
RUN apt-get install -y npm
RUN npm install -g npm
RUN npm install gulp -g
RUN npm install -g bower
#RUN npm install -g yo
#RUN npm install -g nodemon

RUN mkdir /app
#RUN mkdir /install

COPY default /etc/nginx/sites-available/default
#COPY entrypoint.sh /install/entrypoint.sh

EXPOSE 80
EXPOSE 22

VOLUME /app
WORKDIR /app

#ENTRYPOINT ["/install/entrypoint.sh"]

# Append "daemon off;" to the configuration file
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

CMD export TERM=xterm && service php7.0-fpm start && service nginx start